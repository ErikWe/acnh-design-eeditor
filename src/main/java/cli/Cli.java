package cli;

import cli.commands.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class Cli {
    private final Logger logger = LogManager.getLogger(Cli.class);

    public Cli() {
        this.run();
    }

    private void run() {
        Scanner scanner = new Scanner(System.in);
        String readString = scanner.nextLine();
        while(readString != null) {
            logger.info(String.format("User input: %s", readString));
            String[] args = readString.split(" ");
            CommandName commandName = CommandName.fromString(args[0]);
            switch (commandName) {
                case HELP -> {
                    Command command_help = new Command_Help();
                    command_help.executeCommand(args);
                }
                case EXIT -> {
                    Command command_exit = new Command_Exit();
                    command_exit.executeCommand(args);
                }
                case READ -> {
                    Command command_read = new Command_Read();
                    command_read.executeCommand(args);
                }
                case WRITE -> {
                    Command command_write = new Command_Write();
                    command_write.executeCommand(args);
                }
                case SHRINK -> {
                    Command command_shrink = new Command_Shrink();
                    command_shrink.executeCommand(args);
                }
                case UNKNOWN -> System.out.println("Unknown command");
                default -> {
                    // should not be reached
                }
            }

            if (scanner.hasNextLine()) {
                readString = scanner.nextLine();
            } else {
                readString = null;
            }
        }
    }
}
