package cli.commands;

import design.DesignEditor;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Command_Read implements Command {

    private static final Logger logger = LogManager.getLogger(Command_Read.class);
    private static final Option option_help = new Option("h", "help", false, "Lists available arguments");
    private static final Option option_path = new Option("p", "path", true, "Path of image to import");
    @Override
    public Options getCommandOptions() {
        Options options = new Options();
        options.addOption(option_help);
        options.addOption(option_path);
        return options;
    }

    @Override
    public void executeCommand(String[] args) {
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(getCommandOptions(), args);
            if (line.hasOption(option_help)) {
                logger.info("Called READ with option --help");
               this.describeCommandOptions();
            } else if (line.hasOption(option_path)) {
                String path = line.getOptionValue(option_path);
                logger.info(String.format("Called READ with option --path and value %s", path));
                DesignEditor.getDesignEditorInstance().readImage(path);
            } else {
                logger.info("Called READ without any option... Nothing to do");
            }
        } catch (ParseException exception) {
            System.err.println(exception.getMessage());
        }
    }

    @Override
    public void describeCommandOptions() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("read", getCommandOptions());
    }
}
