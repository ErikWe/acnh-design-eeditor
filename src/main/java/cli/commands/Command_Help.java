package cli.commands;

import cli.CommandName;
import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.stream.Stream;

public class Command_Help implements Command {

    private static final Logger logger = LogManager.getLogger(Command_Help.class);
    private static final Option option_help = new Option("h", "help", false, "Lists available arguments");
    private static final Option option_all = new Option("a", "all", false, "List all available commands");

    @Override
    public Options getCommandOptions() {
        Options options = new Options();
        options.addOption(option_help);
        options.addOption(option_all);
        return options;
    }

    @Override
    public void executeCommand(String[] args) {
        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine line = parser.parse(getCommandOptions(), args);
            if (line.hasOption(option_all)) {
                logger.info("Called HELP with option --all");
                printCommands(Arrays.stream(CommandName.values()).sorted());
            } else if (line.hasOption(option_help)) {
                logger.info("Called HELP with option --help");
                describeCommandOptions();
            } else {
                logger.info("Called HELP without options");
                printCommands(Arrays.stream(CommandName.values()).sorted().filter(commandName -> commandName.mainCommand));
            }
        } catch (ParseException exception) {
            System.err.println(exception.getMessage());
        }
    }

    @Override
    public void describeCommandOptions() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("help", getCommandOptions());
    }
    private void printCommands(Stream<CommandName> commandNameStream) {
        commandNameStream.forEach(commandName -> System.out.printf("%s - %s%n", commandName.name, commandName.description));
    }
}
