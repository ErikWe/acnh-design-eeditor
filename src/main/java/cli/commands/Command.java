package cli.commands;

import org.apache.commons.cli.Options;

public interface Command {
    public Options getCommandOptions();
    public void executeCommand(String[] args);
    public void describeCommandOptions();
}
