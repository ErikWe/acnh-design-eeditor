import cli.Cli;
import design.DesignEditor;
import gui.MainGui;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;

public class Main {

    private static final Logger logger = LogManager.getLogger(Main.class);
    public static void main(String[] args) {
        logger.info("Starting program");
        logger.info(String.format("Program started with args: %s", Arrays.toString(args)));
        System.out.println("Welcome to the ACNH design editor");
        System.out.println("Use \"help\" to get a list of commands");
        System.out.println("For each command, use --help to get more information");

        // create design-editor instance
        DesignEditor.getDesignEditorInstance();

        if (args[0].equals("--gui")) {
            new MainGui();
        } else {
            new Cli();
        }
    }
}
