package utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class FileUtils {

    private static final Logger logger = LogManager.getLogger(FileUtils.class);

    public static BufferedImage readImage(String pathname) {
        logger.info(String.format("Trying to read image: %s", pathname));
        try {
            File input_file = new File(pathname);
            BufferedImage image = ImageIO.read(input_file);
            logger.info("Successfully imported image");
            System.out.println("Success");
            return image;
        }
        catch (IOException e) {
            logger.error(String.format("Image at %s could not be imported", pathname));
            System.out.println("Error, image could not be imported");
        }
        return null;
    }

    public static void writeImage(String pathname, BufferedImage image) {
        logger.info(String.format("Trying to write image: %s", pathname));
        try {
            File output_file = new File(pathname);
            ImageIO.write(image, "png", output_file);
            logger.info("Successfully exported image");
            System.out.println("Success");
        }
        catch (IOException e) {
            logger.error(String.format("Image could not be exported to %s", pathname));
            System.out.println("Error");
        }
    }

}
