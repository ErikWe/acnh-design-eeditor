package design;

import gui.panels.PreviewPanel;
import net.coobird.thumbnailator.Thumbnails;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.FileUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class DesignEditor {

    private static final Logger logger = LogManager.getLogger(DesignEditor.class);

    private static DesignEditor designEditorInstance = null;

    private BufferedImage image;
    private String imagePath;
    private PreviewPanel previewPanel;

    private DesignEditor() {
        this.imagePath = null;
        this.previewPanel = null;
        readDefaultImageOrNull();
    }

    public static DesignEditor getDesignEditorInstance() {
        if (designEditorInstance == null) {
            designEditorInstance = new DesignEditor();
            logger.info("New instance of DesignEditor has been created");
        }
        return designEditorInstance;
    }

    public void addDisplayPanel(PreviewPanel panel) {
        this.previewPanel = panel;
    }

    public BufferedImage getImage() {
        return this.image;
    }

    public void readImage(String path) {
        this.imagePath = path;
        BufferedImage newImage = FileUtils.readImage(path);
        setImage(newImage);
    }

    public void shrinkImage(int width, int length) {
        if (this.image == null) {
            logger.error("Could not shrink image as there is none");
        } else {
            logger.info("Trying to shrink image...");
            try {
                BufferedImage tempImage = Thumbnails.of(this.image)
                        .size(width, length)
                        .keepAspectRatio(false)
                        .asBufferedImage();
                setImage(tempImage);
                logger.info("...done. Image shrunk successfully");
                System.out.println("Image shrunk successfully");
            } catch (IOException exception) {
                logger.error("Could not shrink image");
                logger.error(exception.getMessage());
            }
        }
    }

    public void writeImage(String path) {
        if (path == null) {
            path = this.imagePath.substring(0, this.imagePath.length() - 4) + "-cropped.png";
        }
        FileUtils.writeImage(path, this.image);
    }

    private void readDefaultImageOrNull() {
        logger.info("Trying to read default image");
        try {
            this.image = ImageIO.read(Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream("default-image.png")));
            logger.info("Successfully red default image");
        } catch (IOException exception) {
            logger.info("Could not read default image");
            logger.error(exception.getMessage());
            this.image = null;
        }
    }

    private void setImage(BufferedImage image) {
        if (image != null) {
            this.image = image;
            if (this.previewPanel != null) {
                this.previewPanel.reloadImage();
            }
        }
    }
}
