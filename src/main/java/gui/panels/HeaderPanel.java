package gui.panels;

import net.miginfocom.swing.MigLayout;

import javax.swing.*;
import java.awt.*;

public class HeaderPanel implements Panel {

    public HeaderPanel() {}

    @Override
    public JPanel get() {
        MigLayout headerLayout = new MigLayout("wrap 1");
        JPanel headerPanel = new JPanel(headerLayout);
        String headerLabelText = "Preview:";
        JLabel headerLabel = new JLabel(headerLabelText);
        headerLabel.setFont(new Font("Default", Font.PLAIN, 20));
        headerPanel.add(headerLabel);
        return headerPanel;
    }
}
