package gui;

import gui.panels.MainPanel;

import javax.swing.*;

public class MainGui  {

    public MainGui() {
        this.run();
    }

    private void run() {
        JFrame frame = new JFrame();
        frame.setTitle("ACNH Design Editor");
        frame.setContentPane(new MainPanel().get());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setResizable(false);
        frame.setVisible(true);
    }
}
